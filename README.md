# Intelligent web
## COM3504 Assignment - Twitter Search Application

### Football transfer rumours app. Built upon Twitter Search & Streaming API, to allow you to search for various tweets published on twitter.com in real-time. Stores data in a database to reduce load and optimise performance.

![app screenshot.png](queryexamples/rooneORmanutdLoading.png)
---------------------------------------------------------------------------------------------------------------
![app screenshot.png](queryexamples/waynerooneyOrManutd.png)
